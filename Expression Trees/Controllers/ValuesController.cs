﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Expression_Trees.Models;
using Microsoft.AspNetCore.Mvc;

namespace Expression_Trees.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private MyDbContext _context { get; }

        private IMapper Mapper { get; set; }

        public ValuesController(MyDbContext context, IMapper mapper)
        {
            _context = context;
            Mapper = mapper;
        }

        [Route("action")]
        public IActionResult Add()
        {
            for (int c = 1; c <= 3; c++)
            {
                var Car = new Car()
                {
                    Name = Enumerable.Range(1, 6)
                        .SelectMany(x => new Random().Next(1, 10).ToString())
                        .Aggregate("", (sum, item) => sum + item.ToString()),
                    Price = 12,
                    CategoryId = c
                };
                _context.Cars.Add(Car);
            }

            _context.SaveChanges();
            return Ok();
        }

        [Route("category")]
        public IActionResult AddCategory()
        {
            for (int i = 0; i < 3; i++)
            {
                var Category = new Category()
                {
                    CountCars = 6
                };
                _context.Categories.Add(Category);
            }

            _context.SaveChanges();
            return Ok();
        }

        public IActionResult Get()
        {
           Console.Write(Expressions.Start());
            return _context.Query<Category>().PipeTo(Ok); /*Categories.Where(Category.filter)
                .ProjectTo<CategoryDto>()
                .PipeTo(Ok);*/
        }
        
    }

    public static class Extentions
    {
        public static TResult PipeTo<TKey, TResult>(this TKey key, Func<TKey, TResult> func)
            => func(key);
    }
}