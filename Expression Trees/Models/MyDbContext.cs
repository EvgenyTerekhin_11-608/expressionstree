﻿using Microsoft.EntityFrameworkCore;

namespace Expression_Trees.Models
{
    public class MyDbContext:DbContext
    {
        public MyDbContext(DbContextOptions opt):base (opt)
        {
            
        }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}