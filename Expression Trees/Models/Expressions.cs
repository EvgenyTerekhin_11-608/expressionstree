﻿using System;
using System.Linq.Expressions;

namespace Expression_Trees.Models
{
    public class Expressions
    {
        public static bool Start()
        {
            Console.Write(PredicateGenerate().Compile());
            return PredicateGenerate().Compile()(7);
        }

        public int SQR(int x)
        {
            return x;
        }


        delegate int SQRT(int number);

        public static Expression<Func<int, bool>> PredicateGenerate()
        {
            var num = Expression.Parameter(typeof(int), null);
            var five = Expression.Constant(5, typeof(int));
            var exp = Expression.LessThan(num, five);
            return Expression.Lambda<Func<int, bool>>(
                exp, num);
        }
    }
}