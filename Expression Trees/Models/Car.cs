﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Expression_Trees.Models
{
    public class CarsProfile : Profile
    {
        public CarsProfile()
        {
            CreateMap<Category, CategoryDto>();
        }
    }
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        
        public Category Category { get; set; }
        public int CategoryId { get; set; }

        public static readonly Expression<Func<Car, bool>> Fail = x => x.Price > 20 && x.Name == "Car";
    }

    public class Category
    {
        public Car Car { get; set; }
        
        public int Id { get; set; }
        public int CountCars { get; set;}

        public static Expression<Func<Category, bool>> filter = x => x.Car.Price > 10;
    }

    public class CategoryDto
    {
        public int Id { get; set; }
        public Car Car { get; set; }
    }
}